package com.erp.hrms.service;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.vo.AdminVo;
import com.erp.hrms.vo.ParamVo;

public interface AdminService {

    /**
     * 管理员登录
     *
     * @param paramVo 传入参数
     * @return 信息
     */
    BaseResp login(ParamVo paramVo) throws Exception;

    /**
     * 修改密码
     *
     * @param paramVo 传入参数
     * @return 信息
     */
    BaseResp modifyPwd(ParamVo paramVo);

    /**
     * 管理员新增
     *
     * @param adminVo 传入参数
     * @return 信息
     */
    BaseResp insertAdmin(AdminVo adminVo);

    /**
     * 管理员修改
     *
     * @param adminVo 传入参数
     * @return 信息
     */
    BaseResp modifyAdmin(AdminVo adminVo);


}
