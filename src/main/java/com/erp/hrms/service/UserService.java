package com.erp.hrms.service;


import com.erp.hrms.common.BaseResp;
import com.erp.hrms.entity.User;

public interface UserService {


    /**
     * 按条件查询所有用户
     *
     * @param user
     * @return
     */
    BaseResp selectUser(User user);


    /**
     * 新增用户信息
     *
     * @param user
     * @return
     */
    BaseResp insertUser(User user);


    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    BaseResp modifyUser(User user);


    /**
     * 删除用户信息
     *
     * @param user
     * @return
     */
    BaseResp deleteUser(User user);
}
