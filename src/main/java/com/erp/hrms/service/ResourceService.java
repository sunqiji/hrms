package com.erp.hrms.service;

import com.erp.hrms.common.BaseResp;

public interface ResourceService {

    /**
     * 查询所有权限
     *
     * @return 集合
     */
    BaseResp queryAll();


    /**
     * 根据角色id查询权限
     *
     * @param roleId 角色di
     * @return 集合
     */
    BaseResp selectByRoleId(Integer roleId);
}
