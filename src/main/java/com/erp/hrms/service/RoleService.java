package com.erp.hrms.service;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.entity.Role;
import com.erp.hrms.vo.RoleVo;


public interface RoleService {
    /**
     * 分页查询
     *
     * @param role 实体
     * @return 信息
     */
    BaseResp selectRole(Role role);


    /**
     * 新增角色数据
     *
     * @param role 实体+List
     * @return 信息
     */
    BaseResp insertRole(RoleVo role);


    /**
     * 修改角色数据
     *
     * @param roleVo 实体+List
     * @return 信息
     */
    BaseResp modifyRole(RoleVo roleVo);


    /**
     * 删除角色信息
     *
     * @param role 实体+List
     * @return 信息
     */
    BaseResp deleteRole(RoleVo role);


    /**
     * 全部查询
     *
     * @return 集合
     */
    BaseResp queryAll();

}
