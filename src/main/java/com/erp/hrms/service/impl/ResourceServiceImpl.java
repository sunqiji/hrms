package com.erp.hrms.service.impl;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.dao.ResourceDao;
import com.erp.hrms.dao.RoleResourceDao;
import com.erp.hrms.entity.RoleResource;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.ResourceService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;


@Service("resourceService")
public class ResourceServiceImpl implements ResourceService {
    @Resource
    private ResourceDao resourceDao;
    @Resource
    private RoleResourceDao roleResourceDao;

    @Override
    public BaseResp queryAll() {
        return new BaseResp(CommonEnum.SUCCESS, resourceDao.selectResource());
    }

    @Override
    public BaseResp selectByRoleId(Integer roleId) {
        if (Objects.isNull(roleId)) {
            return new BaseResp(CommonEnum.RESOURCE_QUERY_ERROR);
        }
        List<RoleResource> list = roleResourceDao.queryByRoleId(roleId);
        if (CollectionUtils.isEmpty(list)) {
            return new BaseResp(CommonEnum.RESOURCE_QUERY_ERROR);
        }
        return new BaseResp(CommonEnum.SUCCESS, resourceDao.queryById(list));
    }
}
