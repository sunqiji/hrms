package com.erp.hrms.service.impl;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.dao.UserDao;
import com.erp.hrms.entity.User;
import com.erp.hrms.enums.Atom;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.UserService;
import com.erp.hrms.utils.CharUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Objects;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    @Override
    public BaseResp selectUser(User user) {
        PageHelper.startPage(user.getPageNum(), user.getPageSize());
        Page<User> userPage = (Page<User>) userDao.selectUser(user);
        return new BaseResp(
                CommonEnum.SUCCESS,
                userPage.getResult(),
                userPage.getPageSize(),
                userPage.getPageNum(),
                userPage.getPages(),
                userPage.getTotal());
    }

    @Override
    public BaseResp insertUser(User user) {
        BaseResp baseResp = checkUser(user);
        if (null != baseResp) {
            return baseResp;
        }
        if (userDao.insertUser(user)) {
            return new BaseResp(CommonEnum.SUCCESS);
        }
        return new BaseResp(CommonEnum.USER_INSERT_ERROR);
    }

    @Override
    public BaseResp modifyUser(User user) {
        BaseResp baseResp = checkUser(user);
        if (null != baseResp) {
            return baseResp;
        }
        if (Objects.isNull(user.getId())) {
            return new BaseResp(CommonEnum.USER_ID_ERROR);
        }
        if (userDao.modifyUser(user)) {
            return new BaseResp(CommonEnum.SUCCESS);
        }
        return new BaseResp(CommonEnum.USER_MODIFY_ERROR);
    }


    @Override
    public BaseResp deleteUser(User user) {
        if (Objects.isNull(user.getId())) {
            return new BaseResp(CommonEnum.USER_ID_ERROR);
        }
        if (userDao.deleteUser(user)) {
            return new BaseResp(CommonEnum.SUCCESS);
        }
        return new BaseResp(CommonEnum.USER_DELETE_ERROR);
    }


    private BaseResp checkUser(User user) {
        String phone = user.getPhone();
        String name = user.getUsername();
        String memo = user.getMemo();
        if (!CharUtil.isPhoneLegal(phone) || phone.length() > Atom.CONSTANT_ELEVEN) {
            return new BaseResp(CommonEnum.USER_PHONE_ERROR);
        }
        if (CharUtil.isSpecialChar(name) || name.length() > Atom.CONSTANT_SIXTY_FOUR) {
            return new BaseResp(CommonEnum.USER_NAME_ERROR);
        }
        if (Objects.isNull(user.getSex())) {
            return new BaseResp(CommonEnum.USER_SEX_ERROR);
        }
        if (!StringUtils.isEmpty(memo)) {
            if (CharUtil.isSpecialChar(memo)) {
                return new BaseResp(CommonEnum.USER_MEMO_ERROR);
            }
        }
        if (Objects.isNull(user.getStatus())) {
            return new BaseResp(CommonEnum.USER_STATUS_ERROR);
        }
        return null;
    }

}
