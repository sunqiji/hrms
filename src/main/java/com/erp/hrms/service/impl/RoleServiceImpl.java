package com.erp.hrms.service.impl;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.dao.AdminRoleDao;
import com.erp.hrms.dao.RoleDao;
import com.erp.hrms.dao.RoleResourceDao;
import com.erp.hrms.entity.Role;
import com.erp.hrms.enums.Atom;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.RoleService;
import com.erp.hrms.utils.CharUtil;
import com.erp.hrms.vo.RoleVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Objects;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleDao roleDao;
    @Resource
    private RoleResourceDao roleResourceDao;
    @Resource
    private AdminRoleDao adminRoleDao;

    @Override
    public BaseResp selectRole(Role role) {
        if (CharUtil.isSpecialChar(role.getRoleName())) {
            return new BaseResp(CommonEnum.ROLE_SELECT_ERROR);
        }
        int pageNum = role.getPageNum();
        int pageSize = role.getPageSize();
        if (Objects.isNull(pageNum) || Objects.isNull(pageSize)) {
            return new BaseResp(CommonEnum.ROLE_SELECT_ERROR);
        }
        PageHelper.startPage(role.getPageNum(), role.getPageSize());
        Page<Role> rolePage = (Page<Role>) roleDao.selectRole(role);
        return new BaseResp(
                CommonEnum.SUCCESS,
                rolePage.getResult(),
                rolePage.getPageSize(),
                rolePage.getPageNum(),
                rolePage.getPages(),
                rolePage.getTotal());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResp insertRole(RoleVo role) {
        if (CharUtil.isSpecialChar(role.getRoleName())) {
            return new BaseResp(CommonEnum.ROLE_INSERT_ERROR);
        }
        if (CollectionUtils.isEmpty(role.getList())) {
            return new BaseResp(CommonEnum.ROLE_INSERT_ERROR);
        }
        if (roleDao.insert(role) > Atom.CONSTANT_ZERO && roleResourceDao.insert(role.getList()) > Atom.CONSTANT_ZERO) {
            return new BaseResp(CommonEnum.SUCCESS);
        }
        return new BaseResp(CommonEnum.ROLE_INSERT_ERROR);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResp modifyRole(RoleVo roleVo) {
        if (Objects.isNull(roleVo.getId())) {
            return new BaseResp(CommonEnum.ROLE_MODIFY_ERROR);
        }
        if (CharUtil.isSpecialChar(roleVo.getRoleName())) {
            return new BaseResp(CommonEnum.ROLE_MODIFY_ERROR);
        }
        if (CollectionUtils.isEmpty(roleVo.getList())) {
            return new BaseResp(CommonEnum.ROLE_MODIFY_ERROR);
        }
        if (roleDao.updateByPrimaryKey(roleVo) > Atom.CONSTANT_ZERO
                && roleResourceDao.deleteByPrimaryKey(roleVo.getId()) > Atom.CONSTANT_ZERO
                && roleResourceDao.insert(roleVo.getList()) > Atom.CONSTANT_ZERO) {
            return new BaseResp(CommonEnum.SUCCESS);
        }
        return new BaseResp(CommonEnum.ROLE_MODIFY_ERROR);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResp deleteRole(RoleVo role) {
        Integer roleId = role.getId();
        if (Objects.isNull(roleId)) {
            return new BaseResp(CommonEnum.ROLE_DELETE_ERROR);
        }
        if (CollectionUtils.isEmpty(adminRoleDao.selectByRoleId(roleId))) {
            if (roleDao.deleteByPrimaryKey(roleId) > Atom.CONSTANT_ZERO
                    && roleResourceDao.deleteByPrimaryKey(roleId) > Atom.CONSTANT_ZERO) {
                return new BaseResp(CommonEnum.SUCCESS);
            }
        }
        return new BaseResp(CommonEnum.ROLE_IS_USING);
    }

    @Override
    public BaseResp queryAll() {
        return new BaseResp(CommonEnum.SUCCESS, roleDao.queryAll());
    }


}
