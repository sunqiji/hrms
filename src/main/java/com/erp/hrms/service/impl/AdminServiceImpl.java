package com.erp.hrms.service.impl;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.dao.AdminDao;
import com.erp.hrms.dao.AdminRoleDao;
import com.erp.hrms.entity.Admin;
import com.erp.hrms.entity.AdminRole;
import com.erp.hrms.enums.Atom;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.AdminService;
import com.erp.hrms.utils.CharUtil;
import com.erp.hrms.utils.EncryptUtil;
import com.erp.hrms.vo.AdminVo;
import com.erp.hrms.vo.ParamVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Objects;

import static com.erp.hrms.enums.Atom.LOGIN_TOKEN;

@Slf4j
@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Resource
    private AdminDao adminDao;
    @Resource
    private AdminRoleDao adminRoleDao;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public BaseResp login(ParamVo paramVo) throws Exception {
        if (CharUtil.isSpecialChar(paramVo.getAccount())) {
            return new BaseResp(CommonEnum.ADMIN_ERROR);
        }
        Admin byAccount = adminDao.getByAccount(paramVo);
        if (!StringUtils.isEmpty(paramVo.getPwd())) {
            String encrypt1 = EncryptUtil.encrypt(paramVo.getPwd(), Atom.KEY);
            if (byAccount.getPwd().equals(encrypt1)) {
                HttpSession session = paramVo.getRequest().getSession();
                session.setAttribute(LOGIN_TOKEN, byAccount.getId());
                stringRedisTemplate.opsForValue().set(LOGIN_TOKEN + byAccount.getId(), session.getId());
                return new BaseResp(CommonEnum.SUCCESS);
            }
        }
        return new BaseResp(CommonEnum.ADMIN_PWD_ERROR);
    }

    @Override
    public BaseResp modifyPwd(ParamVo paramVo) {
        BaseResp baseResp = checkPwd(paramVo);
        if (null != baseResp) {
            return baseResp;
        }
        Integer id = paramVo.getId();
        String oldPwd = EncryptUtil.encrypt(paramVo.getOldPwd(), Atom.KEY);
        String newPwd = EncryptUtil.encrypt(paramVo.getNewPwd(), Atom.KEY);
        Admin admin = adminDao.getByPwd(id, oldPwd);
        if (Objects.isNull(admin)) {
            return new BaseResp(CommonEnum.ADMIN_MESSAGE_ERROR);
        }
        if (adminDao.modifyPwd(id, newPwd)) {
            stringRedisTemplate.delete(LOGIN_TOKEN + id);
            return new BaseResp(CommonEnum.ADMIN_PWD_MODIFY_SUCCESS);
        }
        return new BaseResp(CommonEnum.ADMIN_PWD_MODIFY_ERROR);
    }

    @Override
    public BaseResp insertAdmin(AdminVo adminVo) {
        if (CharUtil.isSpecialChar(adminVo.getAccount()) || CharUtil.isSpecialChar(adminVo.getPwd())) {
            return new BaseResp(CommonEnum.ADMIN_ERROR);
        }
        if (Objects.isNull(adminVo.getRoleId())) {
            return new BaseResp(CommonEnum.ROLE_MESSAGE_ERROR);
        }
        if (null != adminDao.getByAccount(adminVo)) {
            return new BaseResp(CommonEnum.ADMIN_MESSAGE_EXIST);
        }
        adminVo.setPwd(EncryptUtil.encrypt(adminVo.getPwd(), Atom.KEY));
        Integer adminId = adminDao.insertAdmin(adminVo);
        AdminRole adminRole = new AdminRole();
        adminRole.setAdminId(adminId);
        adminRole.setRoleId(adminVo.getRoleId());
        if (adminRoleDao.insert(adminRole) > Atom.CONSTANT_ZERO) {
            return new BaseResp(CommonEnum.SUCCESS);
        }
        return new BaseResp(CommonEnum.ADMIN_INSERT_ERROR);
    }

    @Override
    public BaseResp modifyAdmin(AdminVo adminVo) {
        Integer id = adminVo.getId();
        Integer roleId = adminVo.getRoleId();
        String pwd = adminVo.getPwd();
        String account = adminVo.getAccount();
        if (Objects.isNull(id)) {
            return new BaseResp(CommonEnum.ADMIN_ERROR);
        }
        if (Objects.isNull(roleId)) {
            return new BaseResp(CommonEnum.ROLE_MESSAGE_ERROR);
        }
        if (null != adminDao.queryByAccount(id, account)) {
            return new BaseResp(CommonEnum.ADMIN_MESSAGE_EXIST);
        }
        if (!StringUtils.isEmpty(pwd)) {
            adminVo.setPwd(EncryptUtil.encrypt(pwd, Atom.KEY));
        }
        if (adminDao.modifyAdmin(adminVo)) {
            if (adminRoleDao.deleteByAdminId(id)) {
                AdminRole adminRole = new AdminRole();
                adminRole.setAdminId(id);
                adminRole.setRoleId(roleId);
                if (adminRoleDao.insert(adminRole) > Atom.CONSTANT_ZERO) {
                    return new BaseResp(CommonEnum.SUCCESS);
                }
            }
        }
        return new BaseResp(CommonEnum.ADMIN_MODIFY_ERROR);
    }

    private BaseResp checkPwd(ParamVo paramVo) {
        Integer id = paramVo.getId();
        String oldPwd = paramVo.getOldPwd();
        String newPwd = paramVo.getNewPwd();
        if (Objects.isNull(id) ||
                CharUtil.isSpecialChar(oldPwd) || CharUtil.isSpecialChar(newPwd)) {
            return new BaseResp(CommonEnum.ADMIN_MESSAGE_ERROR);
        }
        return null;
    }
}
