package com.erp.hrms.common;

import com.erp.hrms.enums.CommonEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class BaseResp<T> {

    private static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    /**
     * 返回码
     */
    private int code;

    /**
     * 返回信息描述
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String currentTime;

    /**
     * 当前页
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer pageNum;
    /**
     * 分页条数
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer pageSize;

    /**
     * 总页数
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer totalPage;

    /**
     * totalRecord 总条数
     *
     * @return
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long totalRecord;

    public BaseResp() {
    }

    /**
     * @param code    错误码
     * @param message 信息
     * @param data    数据
     */
    public BaseResp(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
        this.currentTime = dateFormat.format(new Date());
    }

    /**
     * @param code    错误码
     * @param message 信息
     */
    public BaseResp(int code, String message) {
        this.code = code;
        this.message = message;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
        this.currentTime = dateFormat.format(new Date());
    }

    /**
     * 不带数据的返回结果
     *
     * @param commonEnum
     */
    public BaseResp(CommonEnum commonEnum) {
        this.code = commonEnum.getErrorCode();
        this.message = commonEnum.getErrorMsg();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
        this.currentTime = dateFormat.format(new Date());
    }

    /**
     * 带数据的返回结果
     *
     * @param commonEnum
     * @param data
     */
    public BaseResp(CommonEnum commonEnum, T data) {
        this.code = commonEnum.getErrorCode();
        this.message = commonEnum.getErrorMsg();
        this.data = data;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
        this.currentTime = dateFormat.format(new Date());
    }

    /**
     * 带分页数据的返回结果
     *
     * @param commonEnum
     * @param data
     */
    public BaseResp(
            CommonEnum commonEnum, T data, int pageSize, int pageNum, int totalPage, Long totalRecord) {
        this.code = commonEnum.getErrorCode();
        this.message = commonEnum.getErrorMsg();
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.totalPage = totalPage;
        this.totalRecord = totalRecord;
        this.data = data;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
        this.currentTime = dateFormat.format(new Date());
    }
}
