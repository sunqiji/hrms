package com.erp.hrms;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@SpringBootApplication
@MapperScan("com.erp.hrms.dao")
@Slf4j
public class Application {

    public static void main(String[] args) {
        try {
            disableAccessWarnings();
            long start = System.currentTimeMillis();
            ConfigurableApplicationContext applicationContext =
                    SpringApplication.run(Application.class, args);
            String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
            Arrays.sort(beanDefinitionNames);
            for (int i = 0; i < beanDefinitionNames.length; i++) {
                log.info("bean[{}]={}", String.format("%03d", i + 1), beanDefinitionNames[i]);
            }
            log.info("===============当前主机信息:{} ===============", getIpInfo());
            log.info(
                    "===============当前系统时间:{} {}===============", getCurrentFullTime(), getDayOfTheWeek());
            ConfigurableEnvironment environment = applicationContext.getEnvironment();
            String[] activeProfiles = environment.getActiveProfiles();
            if (null != activeProfiles && activeProfiles.length != 0) {
                log.info("===============环境{}启动成功===============", activeProfiles[0]);
            }
            long milliseconds = System.currentTimeMillis() - start;
            log.info("===============耗时{}毫秒===============", milliseconds);
        } catch (Exception e) {
            log.info(e.toString());
        }
    }

    /**
     * 忽略非法反射警告 适用于jdk11
     */
    @SuppressWarnings("unchecked")
    public static void disableAccessWarnings() {
        try {
            Class unsafeClass = Class.forName("sun.misc.Unsafe");
            Field field = unsafeClass.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            Object unsafe = field.get(null);
            Method putObjectVolatile =
                    unsafeClass.getDeclaredMethod(
                            "putObjectVolatile", Object.class, long.class, Object.class);
            Method staticFieldOffset = unsafeClass.getDeclaredMethod("staticFieldOffset", Field.class);

            Class loggerClass = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field loggerField = loggerClass.getDeclaredField("logger");
            Long offset = (Long) staticFieldOffset.invoke(unsafe, loggerField);
            putObjectVolatile.invoke(unsafe, loggerClass, offset, null);
        } catch (Exception ignored) {
            log.error(ignored.toString());
        }
    }

    public static String getIpInfo() {
        String ipInfo = null;
        try {
            // 获取本地主机地址对象
            InetAddress ip = InetAddress.getLocalHost();
            // 获取其他主机的地址对象
            ipInfo = "ip = " + ip.getHostAddress() + " , hostName = " + ip.getHostName();
        } catch (UnknownHostException e) {
            log.error(e.toString());
        }
        return ipInfo;
    }

    public static String getCurrentFullTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public static String getDayOfTheWeek() {
        String[][] strArray = {
                {"MONDAY", "一"},
                {"TUESDAY", "二"},
                {"WEDNESDAY", "三"},
                {"THURSDAY", "四"},
                {"FRIDAY", "五"},
                {"SATURDAY", "六"},
                {"SUNDAY", "日"}
        };
        LocalDate currentDate = LocalDate.now();
        String k = String.valueOf(currentDate.getDayOfWeek());
        // 获取行数
        for (int i = 0; i < strArray.length; i++) {
            if (k.equals(strArray[i][0])) {
                k = strArray[i][1];
                break;
            }
        }
        return "星期" + k;
    }

}
