package com.erp.hrms.config;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.enums.CommonEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.erp.hrms.enums.Atom.LOGIN_TOKEN;

//拦截登录失效的请求
@Slf4j
public class RedisSessionInterceptor implements HandlerInterceptor {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //无论访问的地址是不是正确的，都进行登录验证，登录成功后的访问再进行分发，404的访问自然会进入到错误控制器中
        HttpSession session = request.getSession();
        if (session.getAttribute(LOGIN_TOKEN) != null) {
            try {
                //验证当前请求的session是否是已登录的session
                String loginId = stringRedisTemplate.opsForValue().get(LOGIN_TOKEN + session.getAttribute(LOGIN_TOKEN));
                if (loginId != null && loginId.equals(session.getId())) {
                    return true;
                }
            } catch (Exception e) {
                log.error(e.toString());
            }
        }
        response401(response);
        return false;
    }

    private void response401(HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            response.getWriter().print(new BaseResp(CommonEnum.ADMIN_IS_NOT_LOGIN));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
