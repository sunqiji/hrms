package com.erp.hrms.dao;

import com.erp.hrms.entity.AdminRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface AdminRoleDao {

    int deleteByPrimaryKey(Integer id);

    int insert(AdminRole record);

    List<AdminRole> selectByRoleId(Integer roleId);

    boolean deleteByAdminId(Integer id);

}