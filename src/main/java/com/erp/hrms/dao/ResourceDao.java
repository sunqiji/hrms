package com.erp.hrms.dao;

import com.erp.hrms.entity.Resource;
import com.erp.hrms.entity.RoleResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface ResourceDao {

    /**
     * 新增
     *
     * @param record 实体
     * @return 是否成功
     */
    int insertSelective(Resource record);

    /**
     * 修改
     *
     * @param record 实体
     * @return 是否成功
     */
    int updateByPrimaryKeySelective(Resource record);

    /**
     * 查询
     *
     * @return 实体集合
     */
    List<Resource> selectResource();

    /**
     * 根据Id批量查询
     *
     * @param list id集合
     * @return 集合
     */
    List<Resource> queryById(List<RoleResource> list);
}