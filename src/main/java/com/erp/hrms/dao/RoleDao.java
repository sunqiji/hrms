package com.erp.hrms.dao;

import com.erp.hrms.entity.Role;
import com.erp.hrms.vo.RoleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface RoleDao {
    /**
     * 删除
     *
     * @param id 主键
     * @return 是否成功
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 新增
     *
     * @param record 实体
     * @return 是否成功
     */
    int insert(RoleVo record);

    /**
     * 修改
     *
     * @param record 实体
     * @return 是否成功
     */
    int updateByPrimaryKey(Role record);


    /**
     * 查询
     *
     * @param role 实体
     * @return 实体集合
     */
    List<Role> selectRole(Role role);


    /**
     * 根据名称查询判断是否重复
     *
     * @param roleName
     * @return
     */
    Role selectByName(String roleName);


    /**
     * 查询全部角色
     *
     * @return 集合
     */
    List<Role> queryAll();

}