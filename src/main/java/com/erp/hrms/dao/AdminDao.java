package com.erp.hrms.dao;

import com.erp.hrms.entity.Admin;
import com.erp.hrms.vo.AdminVo;

public interface AdminDao {

    /**
     * 获取管理员信息
     *
     * @return 管理员实体
     */
    Admin getByAccount(Admin admin);

    /**
     * 根据id获取管理员信息
     *
     * @param id 主键
     * @return 管理员实体
     */
    Admin getById(String id);

    /**
     * 根据id和pwd查询信息
     *
     * @param id  主键
     * @param pwd 密码
     * @return 实体
     */
    Admin getByPwd(Integer id, String pwd);

    /**
     * 修改管理员密码
     *
     * @param id  主键id
     * @param pwd 新密码
     * @return 是否修改成功
     */
    boolean modifyPwd(Integer id, String pwd);

    /**
     * 新增用户信息
     *
     * @param adminVo
     * @return
     */
    Integer insertAdmin(AdminVo adminVo);

    /**
     * 判断修改时是否存在相同的账号
     *
     * @param id      主键id
     * @param account 账号
     * @return 对象
     */
    Admin queryByAccount(Integer id, String account);

    /**
     * 修改管理员信息
     *
     * @param admin 实体
     * @return 是否成功
     */
    boolean modifyAdmin(Admin admin);
}