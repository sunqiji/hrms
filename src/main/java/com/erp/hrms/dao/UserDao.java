package com.erp.hrms.dao;

import com.erp.hrms.entity.User;

import java.util.List;

public interface UserDao {
    /**
     * 按条件查询全部用户
     *
     * @param user 实体
     * @return 实体集合
     */
    List<User> selectUser(User user);

    /**
     * 新增用户
     *
     * @param user 实体
     * @return 是否成功
     */
    boolean insertUser(User user);


    /**
     * 修改用户信息
     *
     * @param user 实体
     * @return 是否成功
     */
    boolean modifyUser(User user);


    /**
     * 删除用户
     *
     * @param user 实体
     * @return 是否成功
     */
    boolean deleteUser(User user);
}