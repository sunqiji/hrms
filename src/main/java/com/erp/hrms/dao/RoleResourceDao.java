package com.erp.hrms.dao;

import com.erp.hrms.entity.RoleResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface RoleResourceDao {

    int deleteByPrimaryKey(Integer roleId);

    int insert(List<RoleResource> list);

    List<RoleResource> queryByRoleId(Integer roleId);

}