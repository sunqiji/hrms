package com.erp.hrms.controller;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/resource")
@Slf4j
public class ResourceController {
    @Resource
    private ResourceService resourceService;

    @GetMapping("/queryAll")
    public BaseResp queryAll() {
        BaseResp baseResp;
        try {
            baseResp = resourceService.queryAll();
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }

    @GetMapping("/selectByRoleId")
    public BaseResp selectByRoleId(@RequestBody Integer roleId) {
        BaseResp baseResp;
        try {
            baseResp = resourceService.selectByRoleId(roleId);
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }

}
