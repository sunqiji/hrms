package com.erp.hrms.controller;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.entity.Role;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.RoleService;
import com.erp.hrms.vo.RoleVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/role")
@Slf4j
public class RoleController {
    @Resource
    private RoleService roleService;

    @GetMapping("/selectRole")
    public BaseResp selectRole(@RequestBody Role role) {
        BaseResp baseResp;
        try {
            baseResp = roleService.selectRole(role);
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }

    @PostMapping("/insertRole")
    public BaseResp insertRole(@RequestBody RoleVo roleVo) {
        BaseResp baseResp;
        try {
            baseResp = roleService.insertRole(roleVo);
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }

    @PostMapping("/modifyRole")
    public BaseResp modifyRole(@RequestBody RoleVo roleVo) {
        BaseResp baseResp;
        try {
            baseResp = roleService.modifyRole(roleVo);
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }

    @PostMapping("/deleteRole")
    public BaseResp deleteRole(@RequestBody RoleVo roleVo) {
        BaseResp baseResp;
        try {
            baseResp = roleService.deleteRole(roleVo);
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }

    @GetMapping("/queryAll")
    public BaseResp queryAll() {
        BaseResp baseResp;
        try {
            baseResp = roleService.queryAll();
        } catch (Exception e) {
            baseResp = new BaseResp(CommonEnum.FAIL);
            log.error(e.toString());
        }
        return baseResp;
    }
}
