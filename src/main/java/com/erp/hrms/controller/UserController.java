package com.erp.hrms.controller;

import com.erp.hrms.common.BaseResp;
import com.erp.hrms.entity.User;
import com.erp.hrms.enums.CommonEnum;
import com.erp.hrms.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Resource
    private UserService userService;

    @GetMapping("/selectUser")
    public BaseResp selectUser(@RequestBody User user) {
        BaseResp baseResp;
        try {
            baseResp = userService.selectUser(user);
        } catch (Exception e) {
            log.error(e.toString());
            baseResp = new BaseResp(CommonEnum.FAIL);
        }
        return baseResp;
    }

    @PostMapping("/insertUser")
    public BaseResp insertUser(@RequestBody User user) {
        BaseResp baseResp;
        try {
            baseResp = userService.insertUser(user);
        } catch (Exception e) {
            log.error(e.toString());
            baseResp = new BaseResp(CommonEnum.FAIL);
        }
        return baseResp;
    }

    @PostMapping("/modifyUser")
    public BaseResp modifyUser(@RequestBody User user) {
        BaseResp baseResp;
        try {
            baseResp = userService.modifyUser(user);
        } catch (Exception e) {
            log.error(e.toString());
            baseResp = new BaseResp(CommonEnum.FAIL);
        }
        return baseResp;
    }

    @GetMapping("/deleteUser")
    public BaseResp deleteUser(@RequestBody User user) {
        BaseResp baseResp;
        try {
            baseResp = userService.deleteUser(user);
        } catch (Exception e) {
            log.error(e.toString());
            baseResp = new BaseResp(CommonEnum.FAIL);
        }
        return baseResp;
    }
}
