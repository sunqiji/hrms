package com.erp.hrms.vo;

import lombok.Data;

@Data
public class ParentVo {
    /**
     * 当前页数
     */
    private Integer pageNum;
    /**
     * 页容
     */
    private Integer pageSize;
}
