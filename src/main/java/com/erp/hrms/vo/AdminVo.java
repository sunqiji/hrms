package com.erp.hrms.vo;

import com.erp.hrms.entity.Admin;
import lombok.Data;

@Data
public class AdminVo extends Admin {
    private Integer roleId;
}
