package com.erp.hrms.vo;

import com.erp.hrms.entity.Admin;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Data
public class ParamVo extends Admin {
    @JsonProperty("oldPwd")
    private String oldPwd;
    @JsonProperty("newPwd")
    private String newPwd;
    @JsonProperty("request")
    private HttpServletRequest request;
    @JsonProperty("response")
    private HttpServletResponse response;
}
