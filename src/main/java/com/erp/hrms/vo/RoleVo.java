package com.erp.hrms.vo;

import com.erp.hrms.entity.Role;
import com.erp.hrms.entity.RoleResource;
import lombok.Data;

import java.util.List;

@Data
public class RoleVo extends Role {
    private List<RoleResource> list;
}
