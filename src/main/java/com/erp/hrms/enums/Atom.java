package com.erp.hrms.enums;

import java.util.regex.Pattern;

/**
 * 配置参数
 *
 * @author shaolei
 * @date 2020/10/18
 */
public final class Atom {

    public static final Pattern INT_PATTERN = Pattern.compile("[0-9]*");
    /**
     * 常量0
     */
    public static final int CONSTANT_ZERO = 0;
    /**
     * 常量1
     */
    public static final int CONSTANT_ONE = 1;
    /**
     * 常量2
     */
    public static final int CONSTANT_TWO = 2;
    /**
     * 常量3
     */
    public static final int CONSTANT_THREE = 3;
    /**
     * 常量5
     */
    public static final int CONSTANT_FIVE = 5;
    /**
     * 常量9
     */
    public static final int CONSTANT_NINE = 9;
    /**
     * 常量11
     */
    public static final int CONSTANT_ELEVEN = 11;
    /**
     * 常量64
     */
    public static final int CONSTANT_SIXTY_FOUR = 64;
    /**
     * 常量-1
     */
    public static final int CONSTANT_NEGATIVE = -1;
    /**
     * ERROR_MSG
     */
    public static final String ERROR_MSG = "ERROR_MSG=";
    /**
     * 密码加密解密KEY
     */
    public static final String KEY = "DORMITORY";
    /**
     * 用户登录后存储在session中
     */
    public static final String LOGIN_TOKEN = "LOGIN_USER";
}
