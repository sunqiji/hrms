package com.erp.hrms.enums;

/**
 * 返回信息
 */
public enum CommonEnum {

    /* 0为成功 */
    SUCCESS(0, "success"),

    USER_INSERT_ERROR(1, "用户添加失败"),
    USER_MEMO_ERROR(2, "用户备注错误"),
    USER_MODIFY_ERROR(3, "用户修改错误"),
    USER_NAME_ERROR(4, "用户名称错误"),
    USER_PHONE_ERROR(5, "用户手机号错误"),
    USER_SELECT_ERROR(6, "用户查询错误"),
    USER_SEX_ERROR(7, "用户性别错误"),
    USER_STATUS_ERROR(8, "用户状态错误"),
    USER_ID_ERROR(9, "用户id错误"),
    USER_DELETE_ERROR(10, "用户删除错误"),

    ADMIN_ERROR(11, "管理员账号或密码错误"),
    ADMIN_PWD_ERROR(12, "管理员密码错误"),
    ADMIN_MESSAGE_ERROR(18, "管理员信息错误"),
    ADMIN_PWD_MODIFY_ERROR(19, "管理员密码修改错误"),
    ADMIN_PWD_MODIFY_SUCCESS(20, "管理员密码修改成功，请重新登录"),
    ADMIN_SIGN_OUT(21, "退出登录"),
    ADMIN_MESSAGE_EXIST(22, "管理员信息存在请勿重复添加"),
    ADMIN_INSERT_ERROR(24, "管理员信息添加失败"),
    ADMIN_MODIFY_ERROR(25, "管理员信息修改失败"),


    ROLE_SELECT_ERROR(13, "角色查询错误"),
    ROLE_INSERT_ERROR(14, "角色添加错误"),
    ROLE_MODIFY_ERROR(15, "角色修改错误"),
    ROLE_DELETE_ERROR(15, "角色删除错误"),
    ROLE_IS_USING(16, "该角色正在使用无法删除"),
    ROLE_MESSAGE_ERROR(23, "角色信息错误"),


    ADMIN_IS_NOT_LOGIN(001, "用户未登录"),

    RESOURCE_QUERY_ERROR(17, "权限查询错误"),


    /* -1为通用失败 */
    FAIL(-1, "系统错误");

    Integer errorCode;
    String errorMsg;

    private CommonEnum(Integer errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public static CommonEnum getErrorCode(Integer errorCode) {
        for (CommonEnum ec : CommonEnum.values()) {
            if (ec.getErrorCode().equals(errorCode)) {
                return ec;
            }
        }
        return CommonEnum.FAIL;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return "errorCode:" + this.errorCode + " errorMsg:" + this.errorMsg;
    }
}
