package com.erp.hrms.entity;

import com.erp.hrms.vo.ParentVo;
import lombok.Data;

import java.io.Serializable;

/**
 * role
 *
 * @author
 */
@Data
public class Role extends ParentVo implements Serializable {

    private Integer id;

    private String roleName;

    private static final long serialVersionUID = 1L;
}