package com.erp.hrms.entity;

import com.erp.hrms.vo.ParentVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * user
 *
 * @author
 */
@Data
public class User extends ParentVo implements Serializable {
    /**
     * id
     */
    private Integer id;

    /**
     * 姓名
     */
    private String username;

    /**
     * 手机号码(唯一，作为登陆账号)
     */
    private String phone;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 状态(1-有效；2-删除)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String memo;

    private static final long serialVersionUID = 1L;
}