package com.erp.hrms.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * role_resource
 * @author 
 */
@Data
public class RoleResource implements Serializable {
    private Integer id;

    private Integer roleId;

    private Integer resourceId;

    private static final long serialVersionUID = 1L;
}