package com.erp.hrms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * resource
 *
 * @author
 */
@Data
public class Resource implements Serializable {
    /**
     * 资源id
     */
    private Integer id;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * 资源类型
     */
    private Integer resourceType;

    /**
     * 资源访问地址
     */
    private String resourceUrl;

    /**
     * 上级资源id
     */
    private Integer parentId;

    /**
     * 资源级别
     */
    private Integer level;

    private static final long serialVersionUID = 1L;
}