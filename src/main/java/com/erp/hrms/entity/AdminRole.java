package com.erp.hrms.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * admin_role
 * @author 
 */
@Data
public class AdminRole implements Serializable {
    private Integer id;

    private Integer adminId;

    private Integer roleId;

    private static final long serialVersionUID = 1L;
}