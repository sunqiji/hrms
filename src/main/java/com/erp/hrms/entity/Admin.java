package com.erp.hrms.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * admin
 *
 * @author
 */
@Data
public class Admin implements Serializable {
    @JsonProperty("id")
    private Integer id;

    /**
     * 操作员账号
     */
    @JsonProperty("account")
    private String account;

    /**
     * 操作员密码
     */
    @JsonProperty("pwd")
    private String pwd;

    private static final long serialVersionUID = 1L;
}